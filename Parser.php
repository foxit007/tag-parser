<?php

require_once 'ParserInterface.php';
require_once 'library/phpQuery.php';

/**
 * @author David Arakelyan <sredstvo@bk.ru>
 */
 
class Parser implements ParserInterface
{
	/*Функция по url адресу получает страницу,
	  после создает модедь DOM,
	  а дальше идет поиск содержимого 
          по заданному тегу или селектору класса css*/
	  
    public function process(string $url, string $tag):array
    {
        $page = file_get_contents($url);
        $document = phpQuery::newDocument($page);
        $elements = $document->find($tag);
        foreach ($elements  as $key){
			$arr[]=pq($key)->text();	
        } 
	return array ($arr);
    }
}


$tag_parser = new Parser;
$url='http://plavaem.info/';

/*Пример1
echo '<pre>';
print_r ($tag_parser->process($url, p));
*/

/*Пример2
foreach($tag_parser->process($url, p) as $key=>$value){
		foreach($value as $data){
			echo $data . '<br>' . '<hr>';	
		}
}
*/
